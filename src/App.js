
import './App.css';
import Child from './components/Child';
import React from 'react';

class App extends React.Component {
  render() {
  let name= "Philip Ndirangu";
  return (
    <div className="App">
      <Child name={name} />
    </div>
  );
}
}
export default App;
